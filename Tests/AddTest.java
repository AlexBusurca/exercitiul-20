import Ro.Orange.MyJUnitClass;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class AddTest {
    @Test
    public void testAdd() {
        int expected = 10;
        int a = 5;
        int b = 5;
        assertEquals(MyJUnitClass.add(a,b), expected);
    }

}
