import Ro.Orange.MyJUnitClass;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ConcatenateTest {
    @Test
    public void testConcatenate() {
        String expected = "alfabeta";
        String a = "alfa";
        String b = "beta" ;
        assertEquals(MyJUnitClass.concatenate(a,b), expected);
    }
}
